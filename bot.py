import time
import praw
import random
import requests
from mastodon import Mastodon

fetcher = praw.Reddit(client_id = "",
			client_secret = "",
			user_agent = "BrebBaker")

poster = Mastodon(access_token = "",
			api_base_url = "https://botsin.space")

print("Clients initialised.")

urls = []
authors = []
limit = 50

sub = fetcher.subreddit("bread")
for submission in sub.new(limit=limit):
	if ".jpg" in submission.url:
		urls.append(submission.url)
		authors.append(submission.author.name)

print("Images fetched from Reddit.")

loc = random.randint(0,len(urls))


image_data = requests.get(urls[loc]).content

with open("image.jpg","wb") as f:
	f.write(image_data)
	f.close()

print("Downloaded image.")

media = poster.media_post("image.jpg")
poster.status_post("Here's your freshly baked bread!\n\nUploaded by "+authors[loc], media_ids=media)
print("Posted to Mastodon.")
