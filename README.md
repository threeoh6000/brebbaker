# Breb Baker
Crossposts reddit posts to Mastodon.

## Usage
On my system, it is set up to run every day at 6AM and 6PM via  cronjob.

You need to enter Reddit Client ID and Secret and Mastodon access key from the developer's panel.
